package com.company;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main {
    public static void main(String[] args) {

        //initialize chrome driver instance
        WebDriver driver = new ChromeDriver();

        //start a new chrome browser
        driver.get("https://www.google.com");


        //quit driver instance
        //Closes chrome browser
        driver.quit();

    }
}