import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyFirstTest {

    WebDriver driver = new ChromeDriver();

    @Before
    public void setUp(){

        driver.get("https://vancomycin-dosing-calculator.doseme.com.au/#/");

    }
    @Test
    public void EnterPatientDataForm() throws Exception {

            WebElement ele1 = driver.findElement(By.cssSelector("#weight"));
            ele1.clear();
            ele1.sendKeys("75");

            WebElement ele3 = driver.findElement(By.cssSelector("#age"));
            ele3.clear();
            ele3.sendKeys("25");

            driver.findElement(By.cssSelector("button.btn.watermelon.btn-primary")).click();
            Thread.sleep(1000);

            driver.findElement(By.cssSelector("button.btn.steel.btn-primary")).click();
            Thread.sleep(1000);

            WebElement ele = driver.findElement(By.cssSelector("#agreementModal___BV_modal_body_ > form > div > div > div > div > label"));
            ele.click();

            driver.findElement(By.cssSelector("#agreementModal___BV_modal_footer_ > button.btn.btn-primary")).click();

    }


    @After
    public void tearDown() throws InterruptedException {

        Thread.sleep(2000);
        driver.close();

    }
}



